from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings

# quey parts
import numpy as np
import codecs
import glob
from nltk.tokenize import RegexpTokenizer
import math
from collections import Counter, OrderedDict
import json
import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + "\\static\\ir\\"
encodingFileMode = "utf-16-le"
allFiles = glob.glob(dir_path + "news_dataset\\news_dataset\\*.txt")
tokenizer = RegexpTokenizer(r'\w+')
stopWordsFile = dir_path + "stopWordsVietnamese.txt"
stopWords = []
allUniqueWords = set()
numOfDocs = 0
numOfUniqueWords = 0
tfidfDict = []
idfDict = None
ratio = [0.4, 0.2, 0.4]  # title body author

benchmarkQueries = []
with codecs.open(dir_path + "SetQuery.txt", "r", encoding="utf-8") as f:
    benchmarkQueries = f.read().splitlines()

# if query the same with query in benchmark, read relevance docs from benchmark
def benchmarkRelevance(query):
    with codecs.open(dir_path + "benchmark\\"+ query +".txt", "r", encoding="utf-8") as f:
        return f.read().split(",")

# get stop words
with codecs.open(stopWordsFile, mode="r", encoding="utf-8") as f:
    stopWords.extend(f.read().splitlines())

# convert document or query into dictionary
def cleanSentence(sentences):
    return Counter([i for i in tokenizer.tokenize(
        sentences.lower()) if i not in stopWords])

with codecs.open(dir_path + "idf.txt", "r", encoding="utf-8") as f:
    idfDict = json.loads(f.read())

with codecs.open(dir_path + "tfidf.txt", "r", encoding="utf-8") as f:
    tfidfDict = json.loads(f.read())

numOfDocs = len(tfidfDict)
numOfUniqueWords = len(idfDict)

# sentences have to be cleaned first (cleanSentence(doc))
def tf_compute(doc):
    for word, val in doc.items():
        doc[word] = 1 + math.log2(val)
    return doc

def tf_idf_compute(doc, ratio=1):
    for word, val in doc.items():
        try:
            doc[word] = idfDict[word] * (1 + math.log2( doc[word] )) * ratio
        except:
            doc[word] = 0 
    return doc

def cosine_sim_dicts(a, b):
    cos_sim = sum(a[key]*b.get(key, 0) for key in a)/(np.linalg.norm(tuple(a.values()))*np.linalg.norm(tuple(b.values())))
    return cos_sim


# Create your views here.
def index(request):
    template = 'index.html'
    return render(request, template)

def calRecall(pos, rels):
    count = 0
    for i in pos:
        if allFiles[i].split("\\")[-1] in rels:
            count += 1
    return count/len(rels)

def calPrecision(pos, rels, k):
    count = 0
    for i in range(k):
        if allFiles[pos[i]].split("\\")[-1] in rels:
            count += 1
    return count/(k+1)

# Average precision in top k
def calAP(pos, rels, k):
    precision = []
    for i in range(k):
        if allFiles[pos[i]].split("\\")[-1] in rels:
            precision.append(calPrecision(pos,rels, i))
    
    return np.array(precision).sum()/len(precision)

# handle query request
def query(request):
    query = request.GET["query"]
    tfidfQ = tf_idf_compute( cleanSentence(query))
    simList = []
    for i in range(numOfDocs):
        simList.append(cosine_sim_dicts(tfidfQ, tfidfDict[i]))
  
    simList = np.array(simList)
    pos = np.argsort(simList)
    pos = pos[simList[pos] != 0]

    result = []
    if len(pos) == 0:
        return HttpResponse("No result")
    else:
        if query in benchmarkQueries:
            rels = benchmarkRelevance(query)
             
            print("Recall ", calRecall(pos, rels))
            print("AP: ", calAP(pos, rels, 20))

        return JsonResponse([allFiles[i].split("\\")[-1] for i in pos], safe=False)

# Function handle request submiting relevant results used for reweight terms or creating gold standard
# Aborted because no use
def relevance(request):
    # with codecs.open(dir_path + "benchmark\\" + request.GET['query']+".txt", "a", "utf-8") as f:
        # f.write(json.dumps(request.GET.getlist('relevance[]')))
    return HttpResponse("ok")
