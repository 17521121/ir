from django.urls import path
from . import views


urlpatterns = [
    # get UI api
    path('', views.index, name='index'),
    # query api
    path('ir', views.query, name='query'),
    # Function handle request submiting relevant results used for reweight terms or creating gold standard
    # Aborted because no use
    path('relevance', views.relevance, name='relevance'),
]
