import os

try:
    os.mkdir("benchmark")
except:
    pass

encodingFileMode = "utf-16-le"
folders = ["Hiếu", "Hùng", "Trung", "Tuấn_Tiến"]
excep = ["Phúc-Bảo Anh",  "Thịnh-Tiến"]
queries = []
with open("SetQuery.txt", "r", encoding="utf_8") as f:
    queries = f.read().splitlines()

def makeBenchmark(query):
    golden = []
    for folder in folders:
        with open("{}/{}.txt".format(folder, query), "r", encoding="utf_8") as f:
            golden.extend(f.read().split(","))
    with open("{}/{}.txt".format(excep[0], query), "r", encoding=encodingFileMode) as f:
            golden.extend(f.read().split(","))
    with open("{}/{}.txt".format(excep[1], query), "r", encoding="utf_8") as f:
           for i in f.read().split(","):
                golden.append(i+".txt")
   
    for i in golden:
        if i == "":
            golden.remove(i)
            
    with open("benchmark/{}.txt".format(query), "w+", encoding="utf_8") as f:
        f.write(",".join(list(set(golden))))

for i in queries:
    makeBenchmark(i)